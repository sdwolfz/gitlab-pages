module gitlab.com/gitlab-org/gitlab-pages

go 1.12

require (
	github.com/certifi/gocertifi v0.0.0-20190905060710-a5e0173ced67 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fzipp/gocyclo v0.0.0-20150627053110-6acd4345c835
	github.com/getsentry/raven-go v0.1.2 // indirect
	github.com/golang/mock v1.3.1
	github.com/gorilla/context v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.0
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/karrick/godirwalk v1.10.12
	github.com/kr/pretty v0.1.0 // indirect
	github.com/namsral/flag v1.7.4-pre
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/prometheus/client_golang v1.1.0
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	github.com/tomasen/realip v0.0.0-20180522021738-f0c99a92ddce
	github.com/wadey/gocovmerge v0.0.0-20160331181800-b5bfa59ec0ad
	gitlab.com/gitlab-org/labkit v0.0.0-20190902063225-3253d7975ca7
	gitlab.com/lupine/go-mimedb v0.0.0-20180307000149-e8af1d659877
	golang.org/x/crypto v0.0.0-20190911031432-227b76d455e7
	golang.org/x/lint v0.0.0-20190930215403-16217165b5de
	golang.org/x/net v0.0.0-20190909003024-a7b16738d86b
	golang.org/x/sys v0.0.0-20190910064555-bbd175535a8b
	golang.org/x/tools v0.0.0-20191010201905-e5ffc44a6fee
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
